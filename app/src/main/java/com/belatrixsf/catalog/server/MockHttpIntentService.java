package com.belatrixsf.catalog.server;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import com.belatrixsf.catalog.dao.ProductsDAO;
import com.belatrixsf.catalog.persistence.ProductsTable;
import com.belatrixsf.catalog.persistence.ProductsMockWS;
import com.belatrixsf.catalog.util.Broadcasts;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by federicofarina on 07/10/15.
 */
public class MockHttpIntentService extends IntentService {

    public interface Params {
        String PAGE = "page";
        String FILTER = "filter";
        String PAGE_SIZE = "page_size";
    }

    public interface ResponseParams {
        String LAST_PAGE = "should_reload";
    }

    public static final String MOCK_HTTP_SERVICE = "MOCK_HTTP_SERVICE";


    public MockHttpIntentService() {
        super(MOCK_HTTP_SERVICE);
    }

    public MockHttpIntentService(String name) {
        super(name);
    }

    /**
     * Mock service, here we process incoming intents in a worker thread so here we could have and http request and database queries.
     *
     * @param intent
     */

    @Override
    protected void onHandleIntent(Intent intent) {


        int pageNum = intent.getIntExtra(Params.PAGE, 1);
        int pageSize = intent.getIntExtra(Params.PAGE_SIZE, 50);
        String filter = intent.getStringExtra(Params.FILTER);

        int productsCount;
        ProductsDAO productsDAO = new ProductsDAO(getApplicationContext());

        boolean filtering = filter != null;

        if (filtering) {
            productsCount = productsDAO.getProductsCountByName(filter);
        } else {
            productsCount = productsDAO.getProductsCount();
        }

        //Avoid server request if we still have local data to load.
        if (productsCount > pageSize * pageNum) {
            sendUpdateBroadcast(false);
        } else {

            JSONObject response;

            if (filtering) {
                response = ProductsMockWS.getProductsByName(filter, pageNum, pageSize);
            } else {
                response = ProductsMockWS.getProducts(pageNum, pageSize);
            }

            JSONArray productsJsonArray = response.optJSONArray(ProductsMockWS.PRODUCTS);

            int responseLength = productsJsonArray.length();

            if (responseLength > 0) {
                List<ContentValues> valuesList = new ArrayList<>();

                for (int i = 0; i < responseLength; i++) {
                    JSONObject productJson = productsJsonArray.optJSONObject(i);
                    String name = productJson.optString("name");
                    if (!productsDAO.existProduct(name)) {
                        String url = productJson.optString("url");
                        String description = productJson.optString("description");
                        ContentValues values = productsDAO.getValues(name, url, description);
                        valuesList.add(values);
                    }
                }

                ContentValues[] bulkToInsert;
                bulkToInsert = new ContentValues[valuesList.size()];
                valuesList.toArray(bulkToInsert);
                getContentResolver().bulkInsert(ProductsTable.URI, bulkToInsert);
            }

            sendUpdateBroadcast(responseLength != pageSize);
        }
    }

    /**
     * Send update broadcast to client.
     *
     * @param isLastPage true if client should re query db, false otherwise.
     */
    private void sendUpdateBroadcast(boolean isLastPage) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(Broadcasts.NOTIFY_OBSERVERS);
        broadcastIntent.putExtra(ResponseParams.LAST_PAGE, isLastPage);
        sendBroadcast(broadcastIntent);
    }
}
