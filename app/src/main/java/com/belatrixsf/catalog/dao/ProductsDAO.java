package com.belatrixsf.catalog.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.belatrixsf.catalog.domain.Product;
import com.belatrixsf.catalog.persistence.ProductsTable;

/**
 * Created by federicofarina on 07/10/15.
 */
public class ProductsDAO {

    protected final Context mContext;

    public ProductsDAO(Context context) {
        mContext = context.getApplicationContext();
    }

    /**
     * @return Products size in database.
     */
    public int getProductsCount() {
        int count = 0;
        Cursor cursor = null;
        try {
            cursor = mContext.getContentResolver().query(ProductsTable.URI, new String[]{"count(*)"},
                    null,
                    null, null);

            if (cursor.moveToFirst())
                count = cursor.getInt(0);
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return count;
    }

    /**
     * @return Products size in database by name filter.
     */
    public int getProductsCountByName(String filter) {
        int count = 0;
        Cursor cursor = null;
        try {
            cursor = mContext.getContentResolver().query(ProductsTable.URI,
                    new String[]{"count(*)"},
                    ProductsTable.Columns.NAME + "=?",
                    new String[]{filter},
                    null);

            if (cursor.moveToFirst())
                count = cursor.getInt(0);
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return count;
    }

    public boolean existProduct(String product) {
        boolean result = false;
        Cursor hash = mContext.getContentResolver().query(ProductsTable.URI,
                null
                , ProductsTable.Columns.NAME + "=?"
                , new String[]{product},
                null);
        if (hash.getCount() > 0)
            result = true;
        hash.close();
        return result;
    }

    public ContentValues getValues(String name, String url, String description) {
        ContentValues groupContentValues = new ContentValues();
        groupContentValues.put(ProductsTable.Columns.URL, url);
        groupContentValues.put(ProductsTable.Columns.NAME, name);
        groupContentValues.put(ProductsTable.Columns.DESCRIPTION, description);
        return groupContentValues;
    }

    public Product groupFromCursor(Cursor cursor) {
        String url = cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.URL));
        String name = cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.NAME));
        String description = cursor.getString(cursor.getColumnIndex(ProductsTable.Columns.DESCRIPTION));
        return new Product(url, name, description);
    }

}
