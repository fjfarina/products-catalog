package com.belatrixsf.catalog.domain;

import android.os.Parcel;
import android.os.Parcelable;
import com.belatrixsf.catalog.util.JSONSerializer;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by federicofarina on 07/10/15.
 */
public class Product implements Parcelable
        ,JSONSerializer{

    public String mUrl;
    public String mName;
    public String mDescription;

    public static final Creator<Product> CREATOR
            = new Creator<Product>() {
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }
    };


    public Product(String anUrl, String aName, String aDescription) {
        mUrl = anUrl;
        mName = aName;
        mDescription = aDescription;
    }

    public Product(Parcel in) {
        mUrl = in.readString();
        mName = in.readString();
        mDescription = in.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(mUrl);
        parcel.writeString(mName);
        parcel.writeString(mDescription);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getName() {
        return mName;
    }

    public String getDescription() {
        return mDescription;
    }

    @Override
    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.putOpt("url", mUrl);
            jsonObject.putOpt("name", mName);
            jsonObject.putOpt("description", mDescription);
        } catch (JSONException ignored) {

        }
        return jsonObject;
    }
}
