package com.belatrixsf.catalog.ui.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.belatrixsf.catalog.R;
import com.belatrixsf.catalog.persistence.CatalogContentProvider;
import com.belatrixsf.catalog.persistence.ProductsTable;
import com.belatrixsf.catalog.server.MockHttpIntentService;
import com.belatrixsf.catalog.ui.adapter.ProductsAdapter;
import com.belatrixsf.catalog.util.Broadcasts;

/**
 * Created by federicofarina on 07/10/15.
 */
public class CatalogFragment extends Fragment
        implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final int URL_LOADER = 0;

    //Allows something similar to infinite scrolling.
    private static final int OFFSET_TO_RELOAD = 25;

    public static final int RESULTS_BY_PAGE = 50;

    private String mQuery;
    private boolean shouldRequestPage = true;
    private boolean mPerformingRequest;
    private ProductsAdapter mProductsAdapter;
    private LinearLayoutManager mLayoutManager;


    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int adapterCount = mProductsAdapter.getItemCount();
            int lastInScreen = mLayoutManager.findLastVisibleItemPosition();

            if ((dy > 0 //Enable only on down scroll.
                    && !mPerformingRequest //Avoid request if we are already performing one.
                    && shouldRequestPage //If last page response from server was empty we do not request again.
                    && lastInScreen + OFFSET_TO_RELOAD >= adapterCount)) { //Perform request until reach the last item.
                mPerformingRequest = true;
                int pageNum = 1 + adapterCount / RESULTS_BY_PAGE;
                requestProducts(pageNum);
            }
        }
    };


    private BroadcastReceiver updateUICallBackBr = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            shouldRequestPage = !intent.getBooleanExtra(MockHttpIntentService.ResponseParams.LAST_PAGE, false);
            getLoaderManager().restartLoader(URL_LOADER, null, CatalogFragment.this);
        }
    };


    public static Fragment newInstance() {
        return new CatalogFragment();
    }

    public CatalogFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Broadcasts.NOTIFY_OBSERVERS);
        getActivity().registerReceiver(updateUICallBackBr, intentFilter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RecyclerView rv = (RecyclerView) inflater.inflate(
                R.layout.catalog_list, container, false);
        setupRecyclerView(rv);
        rv.addOnScrollListener(onScrollListener);
        return rv;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestProducts(1);
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        mLayoutManager = new LinearLayoutManager(recyclerView.getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        mProductsAdapter = new ProductsAdapter(getContext(), null);
        recyclerView.setAdapter(mProductsAdapter);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        int count = mProductsAdapter.getItemCount();

        Uri queryUri = ProductsTable.URI;
        int offset = count + RESULTS_BY_PAGE;
        queryUri = queryUri.buildUpon().appendQueryParameter(CatalogContentProvider.QUERY_PARAMETER_LIMIT, 0 + "," + offset).build();

        return new CursorLoader(getActivity(),
                queryUri, // URI
                null,  // projection fields
                mQuery == null ? null : ProductsTable.Columns.NAME + " like ?", // the selection criteria
                mQuery == null ? null : new String[]{mQuery + "%"}, // the selection args
                ProductsTable.Columns.NAME // the sort order
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mProductsAdapter.changeCursor(data);
        mPerformingRequest = false;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mProductsAdapter.changeCursor(null);
    }


    private void requestProducts(int page) {
        Intent serviceIntent = new Intent(getActivity(), MockHttpIntentService.class);
        serviceIntent.putExtra(MockHttpIntentService.Params.PAGE, page);
        serviceIntent.putExtra(MockHttpIntentService.Params.FILTER, mQuery);
        serviceIntent.putExtra(MockHttpIntentService.Params.PAGE_SIZE, RESULTS_BY_PAGE);
        getActivity().startService(serviceIntent);
    }

    public void queryResults(String aQuery) {
        restartPaginationState();
        mQuery = aQuery;
        mProductsAdapter.changeCursor(null);
        requestProducts(1);
    }


    public void finishSearch() {
        restartPaginationState();
        mQuery = null;
        mProductsAdapter.changeCursor(null);
        getLoaderManager().restartLoader(URL_LOADER, null, CatalogFragment.this);
    }

    private void restartPaginationState() {
        mPerformingRequest = false;
        shouldRequestPage = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(updateUICallBackBr);
    }
}
