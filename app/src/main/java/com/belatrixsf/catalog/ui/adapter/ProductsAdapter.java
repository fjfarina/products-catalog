package com.belatrixsf.catalog.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.belatrixsf.catalog.R;
import com.belatrixsf.catalog.domain.Product;
import com.belatrixsf.catalog.ui.activity.ProductDetailActivity;
import com.squareup.picasso.Picasso;

/**
 * Created by federicofarina on 06/10/15.
 */
public class ProductsAdapter extends OrmLiteRecyclerViewCursorAdapter {

    public ProductsAdapter(Context context, Cursor cursor) {
        super(context, cursor);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemViewType(Product item, int position) {
        return 0;
    }

    @Override
    protected void bindView(Context context, final RecyclerView.ViewHolder holder, int position, final Product item) {

        final ViewHolder viewHolder = (ViewHolder) holder;

        final String itemName = item.getName();
        viewHolder.mTextView.setText(itemName);

        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.putExtra(ProductDetailActivity.EXTRA_ITEM, item);
                context.startActivity(intent);
            }
        });


        Picasso.with(context)
                .load(item.getUrl())
                .fit()
                .placeholder(R.drawable.ic_info_outline_black_24dp)
                .centerCrop()
                .into(viewHolder.mImageView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final ImageView mImageView;
        public final TextView mTextView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (ImageView) view.findViewById(R.id.avatar);
            mTextView = (TextView) view.findViewById(android.R.id.text1);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTextView.getText();
        }
    }
}

