package com.belatrixsf.catalog.ui.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import com.belatrixsf.catalog.dao.ProductsDAO;
import com.belatrixsf.catalog.domain.Product;

/**
 * Created by federicofarina on 07/10/15.
 */
public abstract class OrmLiteRecyclerViewCursorAdapter extends RecyclerViewCursorAdapter {

    private final ProductsDAO mProductsDAO;

    public OrmLiteRecyclerViewCursorAdapter(Context context, Cursor c) {
        super(context, c);
        mProductsDAO = new ProductsDAO(context);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, Cursor cursor) {
        Product product = mProductsDAO.groupFromCursor(cursor);
        bindView(mContext, viewHolder, cursor.getPosition(), product);
    }

    @Override
    public int getItemViewType(int position) {
        if (!mDataValid)
            throw new IllegalStateException("this should only be called when the cursor is valid");

        if (!mCursor.moveToPosition(position))
            throw new IllegalStateException("couldn't move cursor to position " + position);

        Product product = mProductsDAO.groupFromCursor(mCursor);

        return getItemViewType(product, position);
    }

    public abstract int getItemViewType(Product item, int position);

    protected abstract void bindView(Context mContext, RecyclerView.ViewHolder viewHolder, int position, Product item);
}