package com.belatrixsf.catalog.ui.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import com.belatrixsf.catalog.R;
import com.belatrixsf.catalog.ui.fragment.CatalogFragment;
import com.belatrixsf.catalog.util.KeyboardUtils;

/**
 * Created by federicofarina on 07/10/15.
 */
public class MainActivity extends AppCompatActivity {

    private final String CONTENT_FRAGMENT_TAG = "content_fragment_tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content, CatalogFragment.newInstance(), CONTENT_FRAGMENT_TAG);
        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_actions, menu);

        final MenuItem searchViewItem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) searchViewItem.getActionView();
        searchView.setIconifiedByDefault(false);
        searchView.setIconified(false);
        searchView.setFocusable(true);
        searchView.clearFocus();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        searchView.setLayoutParams(params);

        MenuItemCompat.setOnActionExpandListener(searchViewItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                searchView.requestFocus();
                KeyboardUtils.toggleKeyboard(MainActivity.this);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                KeyboardUtils.toggleKeyboard(MainActivity.this);
                reloadResults();
                return true;
            }
        });

        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            CatalogFragment fragment = (CatalogFragment) getSupportFragmentManager().findFragmentByTag(CONTENT_FRAGMENT_TAG);
            if (fragment != null)
                fragment.queryResults(query);
        }
    }

    private void reloadResults() {
        CatalogFragment fragment = (CatalogFragment) getSupportFragmentManager().findFragmentByTag(CONTENT_FRAGMENT_TAG);
        if (fragment != null)
            fragment.finishSearch();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
