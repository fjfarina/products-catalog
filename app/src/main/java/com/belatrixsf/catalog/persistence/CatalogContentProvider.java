package com.belatrixsf.catalog.persistence;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

/**
 * Created by federicofarina on 07/10/15.
 */
public class CatalogContentProvider extends ContentProvider {

    public static final int INSERTION_ERROR = -1;
    public static final String QUERY_PARAMETER_LIMIT = "limit";

    private static final String BASE_PATH = "catalog";
    private static final String AUTHORITY = "com.belatrixsf.catalog.contentprovider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

    private SQLiteHelper databaseHelper;

    @Override
    public boolean onCreate() {
        databaseHelper = new SQLiteHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        String limit = uri.getQueryParameter(QUERY_PARAMETER_LIMIT);

        // Using SQLiteQueryBuilder instead of query() method
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(ProductsTable.TABLE_NAME);

        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        Cursor cursor;

        try {
            db.beginTransaction();
            cursor = queryBuilder.query(db, projection, selection,
                    selectionArgs, null, null, sortOrder, limit);
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        long id = INSERTION_ERROR;

        try {
            db.beginTransaction();
            id = db.insert(ProductsTable.TABLE_NAME, null, values);
            getContext().getContentResolver().notifyChange(uri, null);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    public String getType(Uri uri) {
        return null;
    }
}
