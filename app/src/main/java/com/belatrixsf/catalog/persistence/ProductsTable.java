package com.belatrixsf.catalog.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

/**
 * Created by Fede on 11/12/14.
 */
public class ProductsTable {

    public static final String TABLE_NAME = "products";
    public static final Uri URI = Uri.parse(CatalogContentProvider.CONTENT_URI + "/" + "products");

    public interface Columns {
        String URL = "url";
        String NAME = "name";
        String DESCRIPTION = "description";
    }

    private static final String TABLE_CREATE = "create table "
            + TABLE_NAME + "("
            + Columns.NAME + " text primary key,"
            + Columns.DESCRIPTION + " text not null,"
            + Columns.URL + " text not null"
            + ");";

    public static void onCreate(Context context, SQLiteDatabase database) {
        database.execSQL(TABLE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }
}
