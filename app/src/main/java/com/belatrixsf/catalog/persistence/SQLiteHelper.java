package com.belatrixsf.catalog.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by federicofarina on 07/10/15.
 */
public class SQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "catalog.db";
    private static final int DATABASE_VERSION = 1;

    private final Context mContext;

    boolean isCreating = false;
    SQLiteDatabase currentDB = null;

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public SQLiteDatabase getWritableDatabase() {

        if (isCreating && currentDB != null)
            return currentDB;

        return super.getWritableDatabase();
    }

    @Override
    public SQLiteDatabase getReadableDatabase() {

        if (isCreating && currentDB != null)
            return currentDB;
        return super.getReadableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        isCreating = true;
        currentDB = database;
        ProductsTable.onCreate(mContext, database);
        isCreating = false;
        currentDB = null;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        ProductsTable.onUpgrade(db, oldVersion, newVersion);
        onCreate(db);
    }

}
