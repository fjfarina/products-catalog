package com.belatrixsf.catalog.util;

import org.json.JSONObject;

/**
 * Created by federicofarina on 07/10/15.
 */
public interface JSONSerializer {
    JSONObject toJSON();
}
