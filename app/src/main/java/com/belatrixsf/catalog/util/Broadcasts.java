package com.belatrixsf.catalog.util;

/**
 * Created by federicofarina on 07/10/15.
 */
public interface Broadcasts {
    String NOTIFY_OBSERVERS = "notify_observers";
}
